variable "tags" {
  type = map
  default = {
    Name      = "smoke-test-reports"
    App       = "cm-relaoded"
    Env       = "ci"
  }
}

variable "reloaded_s3_bucket" {
  type = string
  description = "s3 bucket name"
  default = "spectrumw-mobile-smoke-test-report"
}
variable "s3-event-notification-topic" {
  type = string
  description = "s3-event-notification-topic"
  default = "s3-event-notification-topic"
}
