resource "aws_iam_role" "test_role" {
  name = "test_role"
  managed_policy_arns = [aws_iam_policy.reloaded_s3_policy.arn]
  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ecs-tasks.amazonaws.com"
        }
      },
    ]
  })

  tags = var.tags
}
resource "aws_iam_policy" "reloaded_s3_policy" {
  name = "reloaded-s3-policy"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action   = ["s3:PutObject","s3:GetObject", "s3:ListObjects"]
        Effect   = "Allow"
        Resource = "arn:aws:s3:::${var.reloaded_s3_bucket}/*"
      },
    ]
  })
}
